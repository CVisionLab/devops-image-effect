#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Simple image effect application. Applies vintage effect."""

import cv2
import numpy as np
import argparse


def apply_vintage(input_image, amount):
    """
    Apply vintage image effect.

    Parameters
    ----------
    input_image : ndarray
        Input image as numpy array.
    amount : float
        Effect amount.

    Returns
    -------
    ndarray
        Output image with applied effect.
    """

    # Prepare parameters of color correction.
    scale_r = (2.0 / 8.0 - 1.0) * amount + 1.0
    shift_r = 255 * amount * 1.0 / 2.0
    scale_g = (14.0 / 16.0 - 1.0) * amount + 1.0
    shift_g = 255 * amount * 1.0 / 8.0
    scale_b = (4.0 / 8.0 - 1.0) * amount + 1.0
    shift_b = 255 * amount * 1.0 / 4.0

    # Prepare lookup table for color correction.
    red_table = [0] * 256
    green_table = [0] * 256
    blue_table = [0] * 256
    for i in range(256):
        red_table[i] = int(scale_r * i + shift_r)
        blue_table[i] = int(scale_b * i + shift_b)
        green_table[i] = int(scale_g * i + shift_g)
    lut = np.array([red_table, green_table, blue_table]).T[None]
    output_image = cv2.LUT(input_image, lut)
    return output_image


def main():
    """
    Application entry point.
    """
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('src', help='Path to input image')
    parser.add_argument('dst', help='Path to output image')
    args = parser.parse_args()
    input_image = cv2.imread(args.src, cv2.IMREAD_COLOR)
    if input_image is None:
        print(f'ERROR: Can not load image {args.src}.')
        return
    output_image = apply_vintage(input_image, 1.0)
    if not cv2.imwrite(args.dst, output_image):
        print(f'ERROR: Failed to save result to {args.dst} (check the path).')


if __name__ == '__main__':
    main()
